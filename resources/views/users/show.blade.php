@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>Users details</h1>
<table class = "table table-dark">
    <!-- the table data -->
        <tr>
            <td>Id</td><td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td><td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td><td>{{$user->email}}</td>
        </tr> 
  
        <tr>
           <td>Created</td><td>{{$user->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$user->updated_at}}</td>  
        </tr> 
        <tr>
           <td>Department</td> <td>{{$user->department->name}}</td>    
        </tr>   
        </table>

                                                  

               

@endsection

