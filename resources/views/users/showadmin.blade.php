@extends('layouts.app')

@section('title', 'Users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>Users details</h1>
<table class = "table table-dark">
    <!-- the table data -->
        <tr>
            <td>Id</td><td>{{$user->id}}</td>
        </tr>
        <tr>
            <td>Name</td><td>{{$user->name}}</td>
        </tr>
        <tr>
            <td>Email</td><td>{{$user->email}}</td>
        </tr> 
  
        <tr>
           <td>Created</td><td>{{$user->created_at}}</td>
        </tr>
        <tr>
           <td>Updated</td><td>{{$user->updated_at}}</td>  
        </tr>    
        </table>

        
                <form method="POST" action="{{ route('users.changeDepartmentFromUser') }}">
                    @csrf  
                    <div class="form-group row">
                            <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
                            <div class="col-md-6">
                                <select class="form-control" name="department_id">                                                                         
                                   @foreach ($departments as $department)
                                     <option value="{{ $department->id }}"> 
                                         {{ $department->name }} 
                                     </option>
                                   @endforeach    
                                 </select>
                            </div>
                        </div>
                    <input name="id" type="hidden" value = {{$user->id}} >
                    <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Change department
                                </button>
                            </div>
                   </div>                    
               </form> 
                </div>
                                                        

               

@endsection

