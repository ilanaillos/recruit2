@extends('layouts.app')

@section('title', 'users')

@section('content')
@if(Session::has('notallowed'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif
<h1>List of users</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Name</th><th>Email</th><th>Department</th><th>Role</th><th>Delete</th><th>Details</th><th>Make to manager</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>       
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td>{{$user->roles}}</td>

            
            <td>
               <a href = "{{route('user.delete',$user->id)}}">Delete</a>
            </td>  
              <td>
                 <a href = "{{route('users.show',$user->id)}}">Details</a>
            </td> 
            <td>
               <a href = "{{route('users.makemanager',$user->id)}}">MANAGER </a>
            </td>                                                                                                 
        </tr>
    @endforeach
</table>
@endsection
